const komplett_URL = 'https://www.komplett.no/product/1104425/gaming/gaming-utstyr/gaming-headset/razer-nari-essential-gaming-headset';

const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

const websiteScraper = (url) => {
    request({url, timeout: 5000}, (err, res, html) => {
        if(!err && res.statusCode == 200){
            const $ = cheerio.load(html);

            // Scraping data
            const title = $('h1 > span').text();
            const price = parseInt(($('div.product-price > span').text().split(','))[0]);
            const rating = $('div.product-main-info-review.new-arrangement > a:nth-child(1) > span').attr('title');

            const product = {};
            product['title'] = title;
            product['price'] = price;
            product['rating'] = rating;

            // Parse data to json
            const data = JSON.stringify(product, null, '\t');

            // Sending data to 'computer-data.json'
            fs.writeFileSync('computer-data.json', data, function (err){
                if (err) throw err;
                console.log('Json data saved to file');
              });
        }
        else console.log('Request failed.');
    });
};

websiteScraper(komplett_URL);