# Assignment 3

## Description

NodeJS application for scraping data from the web store komplett.no.

In the project directory you can run: `npm start`.

The following data will be sent to computer-data.json:

1. Product name
2. Price in NOK
3. Rating out of 5